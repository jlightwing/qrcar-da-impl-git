package com.lightwing.qrcar.da.impl.service;

import com.lightwing.qrcar.da.api.model.Vehicle;
import com.lightwing.qrcar.da.api.dao.VehicleMapper;
import com.lightwing.qrcar.da.api.service.VehicleDA;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;

/**
 * @see com.lightwing.qrcar.da.api.service.VehicleDA
 */
@Named
public class VehicleDAImpl implements VehicleDA {

    @Autowired
    private VehicleMapper vehicleMapper;

    /**
     * @see VehicleDA getByCode
     */
    public Vehicle getByCode(String code)
    {
        return vehicleMapper.getByCode(code);
    }
}
