package com.lightwing.qrcar.da.impl.dao;

import com.lightwing.qrcar.da.api.dao.VehicleMapper;
import com.lightwing.qrcar.da.api.model.Vehicle;
import com.lightwing.qrcar.da.impl.service.VehicleDAImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VehicleDAImplTest
{
  @Mock
  private VehicleMapper vehicleMapper;

  @InjectMocks
  private VehicleDAImpl vehicleDAImpl;

  private Fixture fixture;

  public static final BigDecimal PRICE = BigDecimal.ONE;


  @Before
  public void setUp()
  {
    fixture = new Fixture();
  }

  @Test
  public void testGetByCode()
  {
    fixture.givenThereIsAVehicleToReturn();
    fixture.givenThereIsACodeToSend();
    fixture.givenVehicleDaoGetByCodeReturnsPopulatedVehicle();
    fixture.whenWeCallVehicleDaoGetByCode();
    fixture.thenWeAssertVehicleDaoGetByCodeCalled();
    fixture.thenWeAssertReturnedVehicleIsCorrect();
  }

  private class Fixture
  {
    private Vehicle vehicleToReturn;
    private String codeToSend;
    private Vehicle resultVehicle;

    public static final String CODE = "code";
    public static final String DESC = "desc";
    public static final String MAKE = "make";
    public static final String MODEL = "model";


    private void givenThereIsAVehicleToReturn()
    {
      vehicleToReturn = new Vehicle();
      vehicleToReturn.setCode(CODE);
      vehicleToReturn.setDesc(DESC);
      vehicleToReturn.setMake(MAKE);
      vehicleToReturn.setModel(MODEL);
      vehicleToReturn.setPrice(PRICE);
    }

    private void givenVehicleDaoGetByCodeReturnsPopulatedVehicle()
    {
      when(vehicleMapper.getByCode(codeToSend)).thenReturn(vehicleToReturn);
    }

    private void givenThereIsACodeToSend()
    {
      codeToSend = CODE;
    }

    private void whenWeCallVehicleDaoGetByCode()
    {
      resultVehicle = vehicleDAImpl.getByCode(codeToSend);
    }

    private void thenWeAssertVehicleDaoGetByCodeCalled()
    {
      verify(vehicleMapper, times(1)).getByCode(codeToSend);
    }

    private void thenWeAssertReturnedVehicleIsCorrect()
    {
      assertEquals(CODE, resultVehicle.getCode());
      assertEquals(MAKE, resultVehicle.getMake());
      assertEquals(MODEL, resultVehicle.getModel());
      assertEquals(DESC, resultVehicle.getDesc());
      assertEquals(PRICE, resultVehicle.getPrice());
    }
  }
}
