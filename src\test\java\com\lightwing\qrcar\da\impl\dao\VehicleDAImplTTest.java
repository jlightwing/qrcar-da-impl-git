package com.lightwing.qrcar.da.impl.dao;

import com.lightwing.qrcar.da.api.model.Vehicle;
import com.lightwing.qrcar.da.impl.service.VehicleDAImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:application-context-mybatis-test.xml")
public class VehicleDAImplTTest
{

  @Autowired
  private VehicleDAImpl vehicleDAImpl;

  private Fixture fixture;

  public static final BigDecimal PRICE = BigDecimal.valueOf(3999.99);


  @Before
  public void setUp()
  {
    fixture = new Fixture();
  }


  @Test
  public void testGetByCodeSuccess()
  {
    fixture.givenThereIsACodeToSend();
    fixture.whenWeCallVehicleDaImplGetByCode();
    fixture.thenWeAssertVehicleGotByCodeIsCorrect();
  }

  @Test
  public void testGetByCodeNullCode()
  {
    fixture.whenWeCallVehicleDaImplGetByCode();
    fixture.thenWeAssertVehicleGotByCodeIsNull();
  }

  private class Fixture
  {
    private String codeToSend;
    private Vehicle resultVehicle;

    public static final String VALID_CODE = "4yrhf8gu43ydh768";
    public static final String DESC = "Red";
    public static final String MAKE = "Ford";
    public static final String MODEL = "Focus";

    private void givenThereIsACodeToSend()
    {
      codeToSend = VALID_CODE;
    }

    private void whenWeCallVehicleDaImplGetByCode()
    {
      resultVehicle = vehicleDAImpl.getByCode(codeToSend);
    }

    private void thenWeAssertVehicleGotByCodeIsCorrect()
    {
      assertEquals(VALID_CODE, resultVehicle.getCode());
      assertEquals(MAKE, resultVehicle.getMake());
      assertEquals(MODEL, resultVehicle.getModel());
      assertEquals(DESC, resultVehicle.getDesc());
      assertEquals(PRICE, resultVehicle.getPrice());
    }

    public void thenWeAssertVehicleGotByCodeIsNull() {
      assertNull(resultVehicle);
    }
  }
}
